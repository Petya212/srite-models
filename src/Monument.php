<?php

namespace Srite\Models;

class Monument extends Model
{
    protected $guarded = [];
    protected static $indexWith = ['gallery'];
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'monument',
    ];
    protected static $defaults = [
        'title' => 'New Monument'
    ];
    protected static $fields = ['title', 'text_html', 'text_quill'];
    protected static $modelTypeName = 'monument';
    protected static $order = true;
    protected static $orderedMany = ['gallery'];

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
