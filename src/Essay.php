<?php
namespace Srite\Models;

class Essay extends Model
{
    protected $guarded = [];
    protected static $indexWith = ['gallery'];
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'essay',
    ];
    protected static $defaults = [
        'title' => 'New Essay'
    ];
    protected static $orderedMany = ['gallery'];
    protected static $fields = ['title', 'text_html', 'text_quill'];
    protected static $modelTypeName = 'essay';
    protected static $order = true;

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
