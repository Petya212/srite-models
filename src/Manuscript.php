<?php

namespace Srite\Models;

class Manuscript extends Model
{
    protected $guarded = [];
    protected static $indexWith = ['gallery'];
    protected static $fields = ['title', 'text_html', 'text_quill', 'collection_id', 'link'];
    protected static $defaults = [
        'title' => 'New Manuscript'
    ];

    protected static $modelTypeName = 'manuscript';
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'manuscript',
    ];
    protected static $order = true;
    protected static $orderedMany = ['gallery'];

    public static function getByCollection($coll_id) {
        return self::where('collection_id', $coll_id)->with(self::$indexWith)->get();
    }

    public static function postToCollection($coll_id) {
        return self::store(['collection_id' => $coll_id]);
    }

    public function collection() {
        return $this->belongsTo('Srite\Models\Collection');
    }

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
