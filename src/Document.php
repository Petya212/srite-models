<?php

namespace Srite\Models;

class Document extends Model
{
    protected $guarded = [];
    protected static $indexWith = ['gallery'];
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'document',
    ];
    protected static $defaults = [
        'title' => 'New Document'
    ];
    protected static $fields = ['title', 'text_html', 'text_quill'];
    protected static $modelTypeName = 'document';
    protected static $orderedMany = ['gallery'];
    protected static $order = true;

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
