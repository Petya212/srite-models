<?php

namespace Srite\Models;

class Event extends Model
{
    protected $guarded = [];
    protected static $indexWith = ['gallery'];
    protected static $fields = ['title', 'date', 'text_html', 'text_quill'];
    protected static $defaults = [
        'title' => 'New Event'
    ];

    protected static $modelTypeName = 'event';
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'event',
    ];
    protected static $orderedMany = ['gallery'];
    protected static $order = true;

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
