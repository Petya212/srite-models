<?php

namespace Srite\Models;

class Collection extends Model
{
    protected $guarded = [];
    protected static $indexWith = ['gallery', 'location'];
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'collection',
    ];
    protected static $defaults = [
        'title' => 'New Collection'
    ];
    protected static $orderedMany = ['gallery'];
    protected static $fields = ['title', 'text_html', 'text_quill', 'place_id'];
    protected static $modelTypeName = 'collection';
    protected static $order = true;

    public function location() {
        return $this->belongsTo('Srite\Models\Place', 'place_id');
    }

    public function manuscripts() {
        return $this->hasMany('Srite\Models\Manuscript');
    }

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
