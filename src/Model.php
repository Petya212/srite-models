<?php

namespace Srite\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Input;

abstract class Model extends EloquentModel
{
    protected $guarded = [];
    protected static $indexWith = [];
    protected static $linkFields = [];
    protected static $modelTypeName;
    protected static $fields = [];
    protected static $orderedMany = [];
    protected static $defaults = [];
    protected static $order = false;

    public static function index() {
        $query = static::with(static::$indexWith);
        if(static::$order) {
            $query->orderBy('order');
        }
        $results = $query->get();
        if(count(static::$linkFields) > 0) {
            if(!static::$modelTypeName) {
                throw new \Exception('No name set.');
            }
            $results->each(function ($item, $key) {
                $item->linkFields = [
                    'typeName' => static::$modelTypeName,
                    'linkText' => $item[static::$linkFields['text']],
                    'linkUrl' => static::$linkFields['endpoint'] . '/' . $item->id,
                ];
            });
        }

        return $results;
    }

    public static function store($valuesSet = null) {
        $values = [];
        foreach (static::$fields as $fieldName) {
            if(Input::get($fieldName)) {
                $values[$fieldName] = Input::get($fieldName);
            }else if($valuesSet && isset($valuesSet[$fieldName])) {
                $values[$fieldName] = $valuesSet[$fieldName];
            }else if(isset(static::$defaults[$fieldName])) {
                $values[$fieldName] = static::$defaults[$fieldName];
            }
        }

        if(static::$order) {
            $values['order'] = static::index()->max(function($item){
                return $item->order;
            }) + 1;
        }

        $record = static::create($values);
        $record = static::get($record->id);
        return $record;
    }

    public static function put($id) {
        $record = static::find($id);

        $inputs = Input::get();
        foreach (static::$fields as $fieldName) {

            if(array_key_exists($fieldName, $inputs)) {
                $record[$fieldName] = Input::get($fieldName);
            }
        }

        if(static::$order) {
            $record['order'] = Input::get('order');
        }

        $record->save();

        foreach (static::$orderedMany as $fieldName) {
            $gals = Input::get($fieldName);
            $newVal = [];
            for($i = 0; $i < count($gals); $i++) {
                $newVal[$gals[$i]['id']] = ['order' => $i];
            }
            call_user_func(array(static::find($id), $fieldName))->sync($newVal);
        }

        return static::get($record->id);
    }

    public static function destroy($id) {
        $record = static::find($id);
        $record->delete();
        return $record;
    }

    public static function get($id) {
        return static::where('id', $id)->with(static::$indexWith)->first();
    }
}
