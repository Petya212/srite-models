<?php

namespace Srite\Models;

class Place extends Model
{
    protected $guarded = [];
    protected static $modelTypeName = 'place';
    protected static $indexWith = ['gallery'];
    protected static $fields = ['name', 'latitude', 'longitude', 'text_html', 'text_quill'];
    protected static $defaults = [
        'name' => 'New Place'
    ];

    protected static $linkFields = [
        'text' => 'name',
        'endpoint' => 'place',
    ];
    protected static $orderedMany = ['gallery'];
    protected static $order = true;

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
