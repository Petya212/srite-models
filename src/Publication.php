<?php

namespace Srite\Models;

class Publication extends Model
{
    protected $guarded = [];
    protected static $indexWith = ['file'];
    protected static $fields = ['title', 'text_html', 'text_quill', 'file_id'];
    protected static $defaults = [
        'title' => 'New Publication'
    ];

    protected static $modelTypeName = 'publication';
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'publication',
    ];
    protected static $order = true;

    public function file() {
        return $this->belongsTo('Srite\Models\File');
    }
}
