<?php

namespace Srite\Models;

class Person extends Model
{
    protected $guarded = [];

    protected static $modelTypeName = 'person';
    protected static $indexWith = ['gallery'];
    protected static $fields = ['title', 'born', 'born_place', 'died_place', 'cv', 'died', 'text_html', 'text_quill'];
    protected static $defaults = [
        'title' => 'New Person'
    ];
    protected static $linkFields = [
        'text' => 'title',
        'endpoint' => 'person',
    ];
    protected static $orderedMany = ['gallery'];
    protected static $order = true;

    public function gallery() {
        return $this->morphToMany('Srite\Models\Image', 'gallery_images')->withPivot('order')->orderBy('order', 'asc');
    }
}
